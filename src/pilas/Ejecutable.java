package pilas;

public class Ejecutable {
    public static  void main(String args[]){
        System.out.println("Ejercicio de pilas en Java 18.");
        Pila pila = new Pila();
        pila.push(1);
        pila.push(2);
        pila.push(4);
        pila.push(8);
        pila.pop();
        while (!pila.empty()){
            System.out.println(pila.pop());
        }
    }
}
