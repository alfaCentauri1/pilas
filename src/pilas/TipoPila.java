package pilas;

public interface TipoPila {
    boolean empty();
    void push(int dato);
    int pop();
    int peek();
    void clear();
    int size();
}
