package pilas;

public class NodoPila {
    private final int dato;
    private NodoPila siguiente;

    public NodoPila(int dato) {
        this.dato = dato;
        siguiente = null;
    }

    public int getDato() {
        return dato;
    }

    public NodoPila getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoPila siguiente) {
        this.siguiente = siguiente;
    }
}
