package pilas;

public class Pila implements TipoPila{
    private NodoPila ultimo;
    int tamanio;

    public Pila() {
        ultimo = null;
        tamanio = 0;
    }

    @Override
    public boolean empty() {
        return (ultimo == null);
    }

    @Override
    public void push(int dato) {
        NodoPila nuevo = new NodoPila(dato);
        nuevo.setSiguiente(ultimo);
        ultimo = nuevo;
        tamanio++;
    }

    @Override
    public int pop() {
        int encontrado = ultimo.getDato();
        ultimo = ultimo.getSiguiente();
        tamanio--;
        return encontrado;
    }

    @Override
    public int peek() {
        return ultimo.getDato();
    }

    @Override
    public int size() {
        return tamanio;
    }

    @Override
    public void clear() {
        while (!empty()){
            pop();
        }
    }
}
